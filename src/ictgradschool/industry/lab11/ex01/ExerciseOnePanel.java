package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeightButton;
    private JTextField heightInMetresText;
    private JTextField weightInKilogramsText;
    private JTextField bmiText;
    private JTextField maxHealthyWeightText;


    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        calculateBMIButton = new JButton("Calculate BMI");
        calculateHealthyWeightButton = new JButton("Calculate Healthy Weight");
        heightInMetresText = new JTextField(10);
        weightInKilogramsText = new JTextField(10);
        bmiText = new JTextField(10);
        maxHealthyWeightText = new JTextField(10);


        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightInMetresLabel = new JLabel("Height in metres:");
        JLabel weightInKilogramsLabel = new JLabel("Weight in kilograms");
        JLabel bmiLabel = new JLabel("Your Body Mass Index (BMI) is:");
        JLabel maxHealthyWeightLabel = new JLabel("Maximum Healthy Weight for your Height");


        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightInMetresLabel);
        this.add(heightInMetresText);
        this.add(weightInKilogramsLabel);
        this.add(weightInKilogramsText);
        this.add(calculateBMIButton);
        this.add(bmiLabel);
        this.add(bmiText);
        this.add(calculateHealthyWeightButton);
        this.add(maxHealthyWeightLabel);
        this.add(maxHealthyWeightText);


        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHealthyWeightButton.addActionListener(this);
    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        if(event.getSource() == calculateBMIButton) {
            double height = Double.parseDouble(heightInMetresText.getText());
            double weight = Double.parseDouble(weightInKilogramsText.getText());
            double bmi = roundTo2DecimalPlaces(weight / (height * height));
            bmiText.setText(Double.toString(bmi));
        } else if(event.getSource() == calculateHealthyWeightButton) {
            double height = Double.parseDouble(heightInMetresText.getText());
            double maxHeight = roundTo2DecimalPlaces(24.9 * height * height);
            maxHealthyWeightText.setText(Double.toString(maxHeight));
        }



    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}