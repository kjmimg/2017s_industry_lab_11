package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    private JTextField firstTextField;
    private JTextField secondTextField;
    private JTextField resultTextField;
    private JButton addButton;
    private JButton subtractButton;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);
        firstTextField = new JTextField(10);
        secondTextField = new JTextField(10);
        resultTextField = new JTextField(20);
        addButton = new JButton("Add");
        subtractButton = new JButton("Subtract");

        JLabel resultLabel = new JLabel("Result:");

        this.add(firstTextField);
        this.add(secondTextField);
        this.add(addButton);
        this.add(subtractButton);
        this.add(resultLabel);
        this.add(resultTextField);

        addButton.addActionListener(this);
        subtractButton.addActionListener(this);

    }

    public void actionPerformed(ActionEvent event) {
        if(event.getSource() == addButton) {
            double first = Double.parseDouble(firstTextField.getText());
            double second = Double.parseDouble(secondTextField.getText());
            double result = roundTo2DecimalPlaces(first + second);
            resultTextField.setText(Double.toString(result));
        } else if (event.getSource() == subtractButton) {
            double first = Double.parseDouble(firstTextField.getText());
            double second = Double.parseDouble(secondTextField.getText());
            double result = roundTo2DecimalPlaces(first - second);
            resultTextField.setText(Double.toString(result));

        }


    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}